// https://v3.nuxtjs.org/api/configuration/nuxt.config
import colors from 'tailwindcss/colors';

export default defineNuxtConfig({
  modules: [
    "@nuxtjs/tailwindcss",
  ],
  tailwindcss: {
    cssPath: "~/assets/css/tailwind.css",
    configPath: "tailwind.config",
    exposeConfig: false,
    injectPosition: 'last',
    viewer: true,
    config: {
      content: [],
      theme: {
        colors: {
          ...colors,
          primary: colors.green,
          danger: colors.red,
          warning: colors.yellow,
        },
      },
    },
  },
});
